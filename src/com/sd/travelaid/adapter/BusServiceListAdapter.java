package com.sd.travelaid.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.internal.en;
import com.sd.travelaid.R;
import com.sd.travelaid.model.BusRoute;
import com.sd.travelaid.model.MenuItem;
import com.sd.travelaid.model.Stopage;

public class BusServiceListAdapter extends ArrayAdapter<BusRoute> {
	private Context mContext;

	public BusServiceListAdapter(Context context, int textViewResourceId, ArrayList<BusRoute> items) {
		super(context, textViewResourceId, items);
		mContext = context;

	}

	private class ViewHolder {

		TextView title,number;
		
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Log.e("position", "" + position);
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.bus_list_row, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.tv_name);
			holder.number = (TextView) convertView.findViewById(R.id.tv_number);
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();

		
		holder.title.setText(getItem(position).getName());
		String start=getItem(position).getStartLocation();
		String end=getItem(position).getFinishLocation();
		if(start.equals("") && end.equals(""))
		{
			//do noting
		}
		else
		 holder.number.setText(start+" - "+end);

		return convertView;
	}

}

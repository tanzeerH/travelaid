package com.sd.travelaid.adapter;

import java.util.List;

import com.google.android.gms.internal.cp;
import com.sd.travelaid.R;
import com.sd.travelaid.model.Comment;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentListAdapter extends ArrayAdapter<Comment>{

	private Context mContext;
	public CommentListAdapter(Context context,int textViewResourceId,List<Comment> items) {
		super(context,R.layout.row_comment,items);
		mContext = context;
	}
	private class ViewHolder {

		TextView cmnt;
		TextView name;
		
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Log.e("position", "" + position);
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_comment, null);
			holder = new ViewHolder();
			holder.cmnt = (TextView) convertView.findViewById(R.id.tv_cmnt);
			holder.name = (TextView) convertView.findViewById(R.id.tv_name);
	
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();

		
		holder.cmnt.setText(getItem(position).getComment());
		holder.name.setText(getItem(position).getUserName());

		return convertView;
	}

}

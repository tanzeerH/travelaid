package com.sd.travelaid.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sd.travelaid.R;
import com.sd.travelaid.model.MenuItem;

public class MenuListAdapter extends ArrayAdapter<MenuItem> {
	private Context mContext;

	public MenuListAdapter(Context context, int textViewResourceId, ArrayList<MenuItem> items) {
		super(context, textViewResourceId, items);
		mContext = context;

	}

	private class ViewHolder {

		TextView title;
		ImageView icon;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Log.e("position", "" + position);
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.nd_row, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.row_title);
			holder.icon = (ImageView) convertView.findViewById(R.id.row_icon);
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();

		holder.icon.setImageResource(getItem(position).getIconRes());
		holder.title.setText(getItem(position).getName());

		return convertView;
	}

}

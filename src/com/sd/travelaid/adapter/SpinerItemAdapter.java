package com.sd.travelaid.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sd.travelaid.R;
import com.sd.travelaid.model.Stopage;

public class SpinerItemAdapter extends ArrayAdapter<Stopage> {

	public Context context;

	public SpinerItemAdapter(Context context, int textViewResourceId,
			List<Stopage> items) {
		super(context, textViewResourceId, items);
		Log.e("msg", "" + items.size());
		this.context = context;

	}

	private class ViewHolder {

		TextView name;

	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return GetCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return GetCustomView(position, convertView, parent);

	}

	private View GetCustomView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.spinner_row,
					null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.tv_name);
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();
		
		Stopage stopage = getItem(position);

		holder.name.setText(stopage.getName());
		
		return convertView;
	}

}
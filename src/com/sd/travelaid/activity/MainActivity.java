package com.sd.travelaid.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sd.travelaid.R;
import com.sd.travelaid.adapter.MenuListAdapter;
import com.sd.travelaid.fragment.AboutFragment;
import com.sd.travelaid.fragment.JourneyFragment;
import com.sd.travelaid.fragment.LogInFragment;
import com.sd.travelaid.fragment.MyNearestMapFragment;
import com.sd.travelaid.fragment.ProfileFragment;
import com.sd.travelaid.fragment.ServicesFragment;
import com.sd.travelaid.fragment.TrafiStopageListFragment;
import com.sd.travelaid.model.MenuItem;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	public DrawerLayout drawerLayout;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	public ListView lvMenuItem;
	private ArrayList<MenuItem> menuItemList = new ArrayList<MenuItem>();
	public MenuListAdapter menuItemAdpter;
	private boolean isFbButtonClicked = false;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Parse.initialize(MainActivity.this, "bdw5QbHRtEceGKVUqc74rYJDRDvrtgb0yFh1sUai",
				"w9s4G7GFrh2bF6F6NoVDP9mp9pB7A4CZg2pG2Uoj");
		ParsePush.subscribeInBackground("", new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e != null) {
					Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
				} else {
					Log.e("com.parse.push", "failed to subscribe for push", e);
				}
			}
		});
		setContentView(R.layout.activity_main);
		getSHA();
		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			}
		}

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		lvMenuItem = (ListView) findViewById(R.id.listview_drawer);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		populateMenuItemList();
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer,
				R.string.drawer_opened, R.string.drawer_closed) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}

			@Override
			public void onDrawerOpened(View drawerView) {

				super.onDrawerOpened(drawerView);
			}

		};
		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		lvMenuItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				drawerLayout.closeDrawer(lvMenuItem);
				FragmentManager fmanager=getFragmentManager();
				int num=fmanager.getBackStackEntryCount();
				while(num>0)
				{
					fmanager.popBackStack();
					num=num-1;
					
				}

				if (position == 0) {
					ServicesFragment servicesFragment = new ServicesFragment();
					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, servicesFragment);
					fragmentTransaction.commit();

				} else if (position == 1) {
					JourneyFragment journeyFragment = new JourneyFragment();
					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, journeyFragment);
					fragmentTransaction.commit();

				} else if (position == 2) {
					TrafiStopageListFragment tfrFragment = new TrafiStopageListFragment();
					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, tfrFragment);
					fragmentTransaction.commit();

				} else if (position == 3) {
					MyNearestMapFragment mpFragment = new MyNearestMapFragment();
					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, mpFragment);
					// fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

				} else if (position == 4) {
					ParseUser user = ParseUser.getCurrentUser();
					LogInFragment logInFragment = new LogInFragment();
					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					if (user == null)
						fragmentTransaction.replace(R.id.content_frame, logInFragment);
					else {
						fragmentTransaction.replace(R.id.content_frame, new ProfileFragment());
					}
					fragmentTransaction.commit();

				}
				else if (position == 5) {
					postToFb();
				}
				else if (position == 6) {
					AboutFragment aboutFragment=new AboutFragment();
					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, aboutFragment);
					// fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}

			}
		});
		ServicesFragment servicesFragment = new ServicesFragment();
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.content_frame, servicesFragment);
		fragmentTransaction.commit();
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			// To do
			if (session.isOpened()) {
				if (isFbButtonClicked) {
					publishFeedDialog();
					isFbButtonClicked = false;
				}

			}
		}
	}

	private void postToFb() {
		isFbButtonClicked = true;
		Session session = Session.getActiveSession();
		if (session.isOpened()) {

			// toast("session opened");
			publishFeedDialog();

		} else {
			onFbLogin();
		}
	}

	private void publishFeedDialog() {
		Log.e("msg", "feed dialog");
		Bundle params = new Bundle();
		params.putString("caption", "Tarvel with TravelAid More Easily.");
		params.putString("description", "Our App let you know which bus to take o reach your destination."+
				"Know about trafiq and move quickly.");
		params.putString("link", "http://travelaid.net78.net/header.jpg");

		WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(this, Session.getActiveSession(), params))
				.setOnCompleteListener(new OnCompleteListener() {

					@Override
					public void onComplete(Bundle values, FacebookException error) {
						if (error == null) {
							// When the story is posted, echo the success
							// and the post Id.
							final String postId = values.getString("post_id");
							if (postId != null) {
								toast("Story Posted");
							} else {
								toast("Publish cancelled");

							}
						} else if (error instanceof FacebookOperationCanceledException) {
							toast("Publish cancelled");

						} else {
							toast("Error posting story");
						}

					}
				}).build();

		feedDialog.show();
	}

	private void onFbLogin() {
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			OpenRequest openRequest = new OpenRequest(MainActivity.this);
			openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
			openRequest.setPermissions("publish_actions");
			session.openForPublish(openRequest.setCallback(statusCallback));
		} else {
			Session.openActiveSession(this, true, statusCallback);
		}

	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
	}
	private void getSHA() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo("com.sd.travelaid",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}

	}
	@Override
	protected void onResume() {

		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void populateMenuItemList() {
		menuItemList.add(new MenuItem("Services", R.drawable.services));
		menuItemList.add(new MenuItem("Search", R.drawable.search));
		menuItemList.add(new MenuItem("Traffiq", R.drawable.trafiq));
		menuItemList.add(new MenuItem("Around Me", R.drawable.around));
		menuItemList.add(new MenuItem("Profile", R.drawable.profile));
		menuItemList.add(new MenuItem("Share", R.drawable.share));
		menuItemList.add(new MenuItem("About", R.drawable.about));
		menuItemAdpter = new MenuListAdapter(MainActivity.this, R.layout.nd_row, menuItemList);
		lvMenuItem.setAdapter(menuItemAdpter);

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		actionBarDrawerToggle.syncState();
		super.onPostCreate(savedInstanceState);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
		super.onConfigurationChanged(newConfig);
	}

}

package com.sd.travelaid.activity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.sd.travelaid.R;
import com.sd.travelaid.util.Common;
import com.sd.travelaid.util.Constants;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

public class SplashActivity extends Activity implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener{
	private boolean isconnected = false;
	private LocationClient locationclient;
	private LocationRequest locationrequest;
	LocationManager locationManager;
	LocationListener locationListener=new MyLocationListner();
	private ProgressDialog pdDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		pdDialog=Common.createProgressDialog(SplashActivity.this);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				LoactionTrack();
				
			}
		}, 3000);
		
	}
	private void LoactionTrack()
	{
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (Constants.hasInternet(getApplicationContext())) {
			if (!manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

				LocationProviderDisableAlert(SplashActivity.this,"Please Enable Google Location Updates");

			} else
				startLoacationTrack();
		}
		else
			internetAvailabilityAlert(SplashActivity.this,"Please Check Your Internet Connection.");
	}
	public void startLoacationTrack() {
		// location Data
		Log.e("start","start location track");
		locationclient = new LocationClient(this, this, this);
		locationrequest = LocationRequest.create();
		// Use high accuracy
		locationrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		// Set the update interval to 10 seconds
		locationrequest.setInterval(10000);
		// Set the fastest update interval to 3 second
		locationrequest.setFastestInterval(3000);
		locationclient.connect();
		
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		Log.e("msg","connected");
		
		isconnected=true;
		
		locationclient.requestLocationUpdates(locationrequest,locationListener);
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
	private void internetAvailabilityAlert(final Context context, String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
			}
		});
		bld.setNegativeButton("Retry", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				LoactionTrack();
				
			}
		});

		bld.create().show();
	}
	private void LocationProviderDisableAlert(final Context context, String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
			}
		});
		bld.setNegativeButton("Enable", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				//to do
				
	            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
	            startActivityForResult(myIntent, Constants.REQUEST_PROVIDER_ENABLING);
			}
		});

		bld.create().show();
	}
	private class MyLocationListner implements LocationListener
	{
		@Override
		public void onLocationChanged(Location location) {
			Log.e("msg","location changed");
			Constants.myLatitude = location.getLatitude();
			Constants.myLongitude = location.getLongitude();
			Intent intent=new Intent(SplashActivity.this,MainActivity.class);
			startActivity(intent);
			if(pdDialog.isShowing())
				pdDialog.dismiss();
			overridePendingTransition(R.anim.fadein, R.anim.fadeout);
			finish();
			
			
		}
		
	}
	@Override
	protected void onDestroy() {
		if(isconnected)
			locationclient.removeLocationUpdates(locationListener);
		super.onDestroy();
	}
	

}

package com.sd.travelaid.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sd.travelaid.R;
import com.sd.travelaid.model.Bus;
import com.sd.travelaid.model.Stopage;
import com.sd.travelaid.util.Constants;

public class ServiceBusesFragment extends Fragment implements OnClickListener{
	String service_Name;
	int route_Id;
	private ArrayList<Bus> busList = new ArrayList<Bus>();
	private ArrayList<Stopage> stopageList=new ArrayList<Stopage>();
	private GoogleMap googleMap;
	private Button btnPath, btnList, btnBus;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_bus_route_buses, null, false);

		MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.locationmap);
		btnPath = (Button) v.findViewById(R.id.btn_path);
		btnList = (Button) v.findViewById(R.id.btn_list);
		btnBus = (Button) v.findViewById(R.id.btn_bus);
		btnList.setOnClickListener(this);
		btnPath.setOnClickListener(this);
		btnList.setTextSize(25);
		googleMap = mapFragment.getMap();
		
		getArgumentData();
		return v;
	}

	private void setMap() {
		Log.e("log", "setupmap");
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(stopageList.get(0).getLatitude(), stopageList.get(0).getLongitude())).zoom(15)
				.build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(stopageList.get(0).getLatitude(), stopageList.get(0).getLongitude())).title("Start");
		googleMap.addMarker(marker);

		googleMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng latlng) {

				googleMap.setMyLocationEnabled(true);
			}
		});

	}

	private void getArgumentData() {
		service_Name = getArguments().getString("service_name");
		route_Id = getArguments().getInt("route_id");
		getActivity().getActionBar().setTitle(service_Name);
		new GetBusStopages().execute();
	}

	private class GetBusStopages extends AsyncTask<Void, Void, Void> {
		String msg = "";
		String filename = "get_bus_stopages_by_route_id.php";

		@Override
		protected Void doInBackground(Void... params) {
			Log.e("......", "hello");
			String url = Constants.URL_ROOT + filename;
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("route_id", "" + route_Id));
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}
			try {
				HttpResponse response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while ((line = br.readLine()) != null) {
					msg += line;
				}
				Log.e("......", msg);
				msg = msg.substring(0, msg.length() - 146);
				parseJson(msg);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			drawRouteInMap();
			new GetServiceBusLocation().execute();
			super.onPostExecute(result);
		}
		private void drawRouteInMap() {
			PolylineOptions rouOptions = new PolylineOptions();
			rouOptions.color(Color.RED);
			for (int i = 0; i < stopageList.size(); i++) {
				rouOptions.add(new LatLng(stopageList.get(i).getLatitude(), stopageList.get(i).getLongitude()));
				MarkerOptions marker = new MarkerOptions().position(
						new LatLng(stopageList.get(i).getLatitude(), stopageList.get(i).getLongitude())).title(
						stopageList.get(i).getName());
				googleMap.addMarker(marker);

			}
			googleMap.addPolyline(rouOptions);
			setMap();
		}

		private void parseJson(String json) {
			try {
				JSONObject jObject = new JSONObject(json);
				JSONObject j = jObject.getJSONObject("success");
				JSONArray jaArray = j.getJSONArray("bus_stopages");
				for (int i = 0; i < jaArray.length(); i++) {
					Log.e("length", "" + jaArray.length());
					JSONObject t = jaArray.getJSONObject(i);
					Stopage stopage = new Stopage(t.getInt("BusRouteId"), t.getString("name"), t.getDouble("latitude"),
							t.getDouble("longitude"));
					stopageList.add(stopage);
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}

		}	
	}
	private class GetServiceBusLocation extends AsyncTask<Void,Void,Void>
	{
		String msg = "";
		String filename = "get_active_buses.php";
		@Override
		protected Void doInBackground(Void... params) {
			Log.e("......", "hello");
			String url = Constants.URL_ROOT + filename;
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("route_id", "" + route_Id));
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}
			try {
				HttpResponse response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while ((line = br.readLine()) != null) {
					msg += line;
				}
				Log.e("......", msg);
				msg = msg.substring(0, msg.length() - 146);
				parseBusData(msg);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			InsertMarkerInMap();
			super.onPostExecute(result);
		}
		private void parseBusData(String json)
		{
			try {
				JSONObject jObject = new JSONObject(json);
				JSONObject j = jObject.getJSONObject("success");
				JSONArray jaArray = j.getJSONArray("buses");
				for (int i = 0; i < jaArray.length(); i++) {
					Log.e("length", "" + jaArray.length());
					JSONObject t = jaArray.getJSONObject(i);
					Bus bus=new Bus(t.getInt("id"),t.getDouble("lattitude"),t.getDouble("longitude"),t.getLong("time"));
					busList.add(bus);
					
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}

		}
		private void InsertMarkerInMap() {
			for (int i = 0; i < busList.size(); i++) {
				MarkerOptions marker = new MarkerOptions().position(
						new LatLng(busList.get(i).getLatitude(), busList.get(i).getLongitude())).title(
						getDate(busList.get(i).getTimiInMili(), "dd/MM/yyyy hh:mm:ss.SSS"));
				marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				googleMap.addMarker(marker);
			}
		}
		public  String getDate(long milliSeconds, String dateFormat)
		{
		    // Create a DateFormatter object for displaying date in specified format.
		    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

		    // Create a calendar object that will convert the date and time value in milliseconds to date. 
		     Calendar calendar = Calendar.getInstance();
		     calendar.setTimeInMillis(milliSeconds);
		     return formatter.format(calendar.getTime());
		
		}
	}
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		try {
			MapFragment fragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.locationmap);
			if (fragment != null)
				getFragmentManager().beginTransaction().remove(fragment).commit();

		} catch (IllegalStateException e) {
			// handle this situation because you are necessary will get
			// an exception here :-(
		}
	}


	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_path) {

		} else if (v.getId() == R.id.btn_list) {
				
			ServiceStopageListFragment sliListFragment=new ServiceStopageListFragment();
			Bundle bundle=new Bundle();
			bundle.putString("service_name",service_Name);
			bundle.putInt("route_id",route_Id);
			sliListFragment.setArguments(bundle);
			FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
			fragmentTransaction.hide(ServiceBusesFragment.this);
			fragmentTransaction.add(R.id.content_frame,sliListFragment);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
		} else if (v.getId() == R.id.btn_bus) {

		}

	}
}

	
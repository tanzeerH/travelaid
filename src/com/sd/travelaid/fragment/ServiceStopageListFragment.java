package com.sd.travelaid.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.GoogleMap;
import com.sd.travelaid.R;
import com.sd.travelaid.adapter.BusServiceStopageListAdapter;
import com.sd.travelaid.model.Stopage;
import com.sd.travelaid.util.Common;
import com.sd.travelaid.util.Constants;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class ServiceStopageListFragment extends Fragment implements OnClickListener{
	
	String service_Name;
	int route_Id;
	private ArrayList<Stopage> stopageList = new ArrayList<Stopage>();
	private BusServiceStopageListAdapter bsadAdapter;
	private Button btnPath, btnList;
	private ListView lvStopage;
	private ProgressDialog pdDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_bus_stopage_list, null, false);
		btnPath = (Button) v.findViewById(R.id.btn_path);
		btnList = (Button) v.findViewById(R.id.btn_list);
		btnPath.setOnClickListener(this);
		btnList.setTextSize(25);
		lvStopage=(ListView)v.findViewById(R.id.lv_stopages);
		bsadAdapter=new BusServiceStopageListAdapter(getActivity(),R.layout.bus_list_row,stopageList);
		lvStopage.setAdapter(bsadAdapter);
		pdDialog=Common.createProgressDialog(getActivity());
		getArgumentData();
		return v;
	}
	private void getArgumentData() {
		service_Name = getArguments().getString("service_name");
		route_Id = getArguments().getInt("route_id");
		getActivity().getActionBar().setTitle(service_Name);
		new GetBusStopages().execute();
	}
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_path) {
				
			FragmentTransaction frTransaction=getFragmentManager().beginTransaction();
			frTransaction.hide(ServiceStopageListFragment.this);
			getFragmentManager().popBackStackImmediate();
			frTransaction.commit();
		} else if (v.getId() == R.id.btn_list) {

		} 
	}
	private class GetBusStopages extends AsyncTask<Void, Void, Void> {
		String msg = "";
		String filename = "get_bus_stopages_by_route_id.php";

		@Override
		protected Void doInBackground(Void... params) {
			Log.e("......", "hello");
			String url = Constants.URL_ROOT + filename;
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("route_id", "" + route_Id));
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}
			try {
				HttpResponse response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while ((line = br.readLine()) != null) {
					msg += line;
				}
				Log.e("......", msg);
				msg = msg.substring(0, msg.length() - 146);
				parseJson(msg);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			bsadAdapter.notifyDataSetChanged();
			pdDialog.dismiss();
			super.onPostExecute(result);
		}

		private void parseJson(String json) {
			try {
				JSONObject jObject = new JSONObject(json);
				JSONObject j = jObject.getJSONObject("success");
				JSONArray jaArray = j.getJSONArray("bus_stopages");
				for (int i = 0; i < jaArray.length(); i++) {
					Log.e("length", "" + jaArray.length());
					JSONObject t = jaArray.getJSONObject(i);
					Stopage stopage = new Stopage(t.getInt("BusRouteId"), t.getString("name"), t.getDouble("latitude"),
							t.getDouble("longitude"));
					stopageList.add(stopage);
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}

		}

	}


}

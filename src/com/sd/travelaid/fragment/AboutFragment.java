package com.sd.travelaid.fragment;

import com.sd.travelaid.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class AboutFragment extends Fragment{
	private WebView wv;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_about,null,false);
		wv=(WebView)v.findViewById(R.id.wv);
		wv.loadUrl("file:///android_asset/about.html");
		return v;
	}

}

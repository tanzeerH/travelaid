package com.sd.travelaid.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.internal.fr;
import com.sd.travelaid.R;
import com.sd.travelaid.adapter.BusServiceListAdapter;
import com.sd.travelaid.model.BusRoute;
import com.sd.travelaid.util.Common;
import com.sd.travelaid.util.Constants;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsPromptResult;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ServicesFragment extends Fragment {
	ListView listView;
	BusServiceListAdapter adapter;
	ArrayList<BusRoute> list=new ArrayList<BusRoute>();
	ProgressDialog pdDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_services,null,false);
		pdDialog=Common.createProgressDialog(getActivity());
		//pdDialog.show();
		listView=(ListView)v.findViewById(R.id.listView1);
		//initBusSerVices();
		adapter=new BusServiceListAdapter(getActivity(),R.layout.bus_list_row,list);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				
				BusRouteMapFragment buFragment=new BusRouteMapFragment();
				Bundle bundle=new Bundle();
				bundle.putString("service_name",list.get(position).getName());
				bundle.putInt("service_id",list.get(position).getServiceId());
				bundle.putInt("route_id",list.get(position).getRouteId());
				buFragment.setArguments(bundle);
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				fragmentTransaction.hide(ServicesFragment.this);
				fragmentTransaction.add(R.id.content_frame,buFragment);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});
		new GetServices().execute();
		return v;
	}
	private class GetServices extends AsyncTask<Void,Void,Void>
	{

		String msg="";
		String filename="get_bus_routes.php";
		@Override
		protected Void doInBackground(Void... params) {
			
			String url=Constants.URL_ROOT+filename;
			DefaultHttpClient client= new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(url);
			try {
				HttpResponse response=client.execute(httpPost);
				HttpEntity entity=response.getEntity();
				InputStream is= entity.getContent();
				BufferedReader br=new BufferedReader(new InputStreamReader(is));
				String line="";
				while((line=br.readLine())!=null)
				{
					msg+=line;
				}
				Log.e("......",msg);
				msg=msg.substring(0,msg.length()-146);
				parseJson(msg);
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			adapter.notifyDataSetChanged();
			pdDialog.dismiss();
			super.onPostExecute(result);
			
		}
		
	}
	private void parseJson(String json)
	{
		try {
			JSONObject jObject=new JSONObject(json);
			JSONObject j=jObject.getJSONObject("success");
			JSONArray jaArray=j.getJSONArray("bus_routes");
			for(int i=0;i<jaArray.length();i++)
			{
				Log.e("length",""+jaArray.length());
				JSONObject t=jaArray.getJSONObject(i);
				BusRoute route=new BusRoute(t.getString("name"),t.getInt("RouteId"),t.getInt("BusServiceId"),t.getString("start"),t.getString("finish"));
				list.add(route);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

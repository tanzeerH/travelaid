package com.sd.travelaid.fragment;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.internal.cm;
import com.google.android.gms.internal.u;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sd.travelaid.R;
import com.sd.travelaid.adapter.CommentListAdapter;
import com.sd.travelaid.model.Comment;
import com.sd.travelaid.util.Common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class ProfileFragment extends Fragment implements OnClickListener{
	private Button btnLogOut,btnPostCmnt;
	private TextView tvName;
	private ListView lvComments;
	private ProgressDialog pdDialog;
	private CommentListAdapter commentListAdapter;
	private ArrayList<Comment> cmntList=new ArrayList<Comment>();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.profile,container,false);
		pdDialog=Common.createProgressDialog(getActivity());
		btnLogOut=(Button)v.findViewById(R.id.btn_logout);
		btnPostCmnt=(Button)v.findViewById(R.id.btn_cmnt);
		tvName=(TextView)v.findViewById(R.id.tv_name);
		lvComments=(ListView)v.findViewById(R.id.lv_cmnts);
		commentListAdapter=new CommentListAdapter(getActivity(),R.layout.row_comment,cmntList);
		lvComments.setAdapter(commentListAdapter);
		setUserName();
		btnLogOut.setOnClickListener(this);
		btnPostCmnt.setOnClickListener(this);
		getComments();
		return v;
	}
	private void setUserName()
	{
		ParseUser user=ParseUser.getCurrentUser();
		if(user!=null)
			tvName.setText(user.getUsername());
	}
	private void logOut()
	{
		ParseUser.logOut();
	}
	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.btn_logout)
		{
			logOut();
			ParseUser user=ParseUser.getCurrentUser();
			FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
			if(user==null)
				fragmentTransaction.replace(R.id.content_frame,new LogInFragment());
			fragmentTransaction.commit();
		}
		else 
			if(v.getId()==R.id.btn_cmnt)
			{
				showPostDialog();
			}
		
	}
	private void showPostDialog() {
		final Dialog infoDialog = new Dialog(getActivity());
		infoDialog.setContentView(R.layout.dialog_post_comment);
		infoDialog.setTitle("Bus Info");
		Button btnPost = (Button) infoDialog.findViewById(R.id.btn_post);
		final EditText edtPost=(EditText)infoDialog.findViewById(R.id.et_cmnt);
		btnPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				infoDialog.dismiss();
				pdDialog.show();
				String cmnt=edtPost.getText().toString();
				ParseObject comment=new ParseObject("Comment");
				comment.put("comment",cmnt);
				ParseUser user=ParseUser.getCurrentUser();
				comment.put("name",user.getUsername());
				comment.put("email",user.getEmail());
				
				comment.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException e) {
						if(e==null)
						{
							//pdDialog.dismiss();
							alert("Comment Posted.");
							commentListAdapter.clear();
							cmntList.clear();
							getComments();
							
						}
						else
							alert("Error in Posting Comment");
						
					}
				});
				
			}
		});
		infoDialog.show();
	}
	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		bld.create().show();
	}
	private void getComments()
	{
		ParseQuery<ParseObject> query=ParseQuery.getQuery("Comment");
		query.addDescendingOrder("createdAt");
		query.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if(e==null){
					for(int i=0;i<objects.size();i++)
					{
						cmntList.add(new Comment(objects.get(i).getString("name"),objects.get(i).getString("comment")));
					}
					commentListAdapter.notifyDataSetChanged();
					pdDialog.dismiss();
				}
				
			}
		});
	}

}

package com.sd.travelaid.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sd.comparator.SortStopagesByName;
import com.sd.travelaid.R;
import com.sd.travelaid.adapter.BusServiceStopageListAdapter;
import com.sd.travelaid.model.Stopage;
import com.sd.travelaid.util.Common;
import com.sd.travelaid.util.Constants;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class TrafiStopageListFragment extends Fragment{
	private ArrayList<Stopage> stopageList = new ArrayList<Stopage>();
	private BusServiceStopageListAdapter bsAdapter;
	private ListView lvList;
	private ProgressDialog pdDialog;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_trafiq_stopage_list, null, false);
		pdDialog=Common.createProgressDialog(getActivity());
		lvList=(ListView)v.findViewById(R.id.lv_slist);
		bsAdapter=new BusServiceStopageListAdapter(getActivity(),R.layout.fragment_bus_stopage_list,stopageList);
		lvList.setAdapter(bsAdapter);
		lvList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				TarfiqMapFragment taFragment=new TarfiqMapFragment();
				Bundle bundle=new Bundle();
				bundle.putString("stopage_name",stopageList.get(position).getName());
				bundle.putDouble("lat",stopageList.get(position).getLatitude());
				bundle.putDouble("lon",stopageList.get(position).getLongitude());
				taFragment.setArguments(bundle);
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				fragmentTransaction.hide(TrafiStopageListFragment.this);
				fragmentTransaction.add(R.id.content_frame,taFragment);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
				
			}
			
		});
		new GetStopages().execute();
		return v;
	}
	private class GetStopages extends AsyncTask<Void, Void, Void> {

		String msg = "";
		String filename = "get_stopages.php";

		@Override
		protected Void doInBackground(Void... params) {
			Log.e("......", "fetching stopages....");
			String url = Constants.URL_ROOT + filename;
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			try {
				HttpResponse response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while ((line = br.readLine()) != null) {
					msg += line;
				}
				Log.e("......", msg);
				msg = msg.substring(0, msg.length() - 146);
				parseJson(msg);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			pdDialog.dismiss();
		bsAdapter.notifyDataSetChanged();
			super.onPostExecute(result);
		}
		private void parseJson(String json)
		{
			try {
				JSONObject jObject=new JSONObject(json);
				JSONObject j=jObject.getJSONObject("success");
				JSONArray jaArray=j.getJSONArray("bus_stopages");
				for(int i=0;i<jaArray.length();i++)
				{
					Log.e("length",""+jaArray.length());
					JSONObject t=jaArray.getJSONObject(i);
					stopageList.add(new Stopage(t.getInt("Id"),t.getString("Name"),t.getDouble("Lattitude"),t.getDouble("Longitude")));
					
				}
				Collections.sort(stopageList, new SortStopagesByName());
				//Collections.sort(list);
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			
		}
	}

}

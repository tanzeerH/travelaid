package com.sd.travelaid.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sd.comparator.SortStopagesByName;
import com.sd.travelaid.R;
import com.sd.travelaid.adapter.SpinerItemAdapter;
import com.sd.travelaid.model.Stopage;
import com.sd.travelaid.util.Common;
import com.sd.travelaid.util.Constants;

import android.R.string;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class JourneyFragment extends Fragment {
	private ArrayList<Stopage> stopageList = new ArrayList<Stopage>();
	private SpinerItemAdapter adapter;
	private Spinner spn1, spn2;
	private ProgressDialog pdDialog;
	private Button btnSearch;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.journey, null, false);
		spn1 = (Spinner) v.findViewById(R.id.spinner1);
		spn2 = (Spinner) v.findViewById(R.id.spinner2);
		btnSearch=(Button)v.findViewById(R.id.btn_search);
		adapter = new SpinerItemAdapter(getActivity(),R.layout.spinner_row,stopageList);
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Bundle bundle=new Bundle();
				int start_id=stopageList.get(spn1.getSelectedItemPosition()).getId();
				int end_id=stopageList.get(spn2.getSelectedItemPosition()).getId();
				bundle.putInt("start_id",start_id);
				bundle.putInt("end_id",end_id);
				SearchResultFragment searchResultFragment=new SearchResultFragment();
				searchResultFragment.setArguments(bundle);
				FragmentTransaction frTransaction=getFragmentManager().beginTransaction();
				frTransaction.hide(JourneyFragment.this);
				frTransaction.add(R.id.content_frame,searchResultFragment);
				frTransaction.show(searchResultFragment);
				frTransaction.addToBackStack(null);
				frTransaction.commit();
				
			}
		});
		spn1.setAdapter(adapter);
		spn2.setAdapter(adapter);
		pdDialog=Common.createProgressDialog(getActivity());
		new GetStopages().execute();
		return v;
	}


	private class GetStopages extends AsyncTask<Void, Void, Void> {

		String msg = "";
		String filename = "get_stopages.php";

		@Override
		protected Void doInBackground(Void... params) {
			Log.e("......", "fetching stopages....");
			String url = Constants.URL_ROOT + filename;
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			try {
				HttpResponse response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while ((line = br.readLine()) != null) {
					msg += line;
				}
				Log.e("......", msg);
				msg = msg.substring(0, msg.length() - 146);
				parseJson(msg);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			pdDialog.dismiss();
			adapter.notifyDataSetChanged();
			super.onPostExecute(result);
		}
		private void parseJson(String json)
		{
			try {
				JSONObject jObject=new JSONObject(json);
				JSONObject j=jObject.getJSONObject("success");
				JSONArray jaArray=j.getJSONArray("bus_stopages");
				for(int i=0;i<jaArray.length();i++)
				{
					Log.e("length",""+jaArray.length());
					JSONObject t=jaArray.getJSONObject(i);
					stopageList.add(new Stopage(t.getInt("Id"),t.getString("Name"),t.getDouble("Lattitude"),t.getDouble("Longitude")));
					
				}
				Collections.sort(stopageList,new SortStopagesByName());
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			
		}
	}

}

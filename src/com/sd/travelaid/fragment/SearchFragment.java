package com.sd.travelaid.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sd.travelaid.R;

@SuppressLint("NewApi")
public class SearchFragment extends Fragment {

	private GoogleMap googleMap;
	View rootView;
	private String juiceBarName = "";
	private double latitude = 23.709;
	private double longitude = 90.407;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.map, null, false);

		
		getActivity().getActionBar().setTitle("Journey");
		MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.locationmap);

		googleMap = mapFragment.getMap();
		setMap();
		return rootView;

	}
	private void setMap() {
		Log.e("log","setupmap");
		CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(15)
				.build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(juiceBarName);
		googleMap.addMarker(marker);
		
		googleMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng latlng) {

				googleMap.setMyLocationEnabled(true);
			}
		});

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		try {
			MapFragment fragment = (MapFragment) getActivity().getFragmentManager()
					.findFragmentById(R.id.locationmap);
			if (fragment != null)
				getFragmentManager().beginTransaction().remove(fragment).commit();

		} catch (IllegalStateException e) {
			// handle this situation because you are necessary will get
			// an exception here :-(
		}
	}
}

package com.sd.travelaid.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import com.sd.travelaid.R;
import com.sd.travelaid.model.Bus;
import com.sd.travelaid.model.NearestBus;
import com.sd.travelaid.model.Stopage;
import com.sd.travelaid.util.Constants;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class TarfiqMapFragment extends Fragment implements OnClickListener {

	String Stopage_Name;
	double lattitude;
	double longitude;
	int route_Id;
	private ArrayList<NearestBus> nBusList = new ArrayList<NearestBus>();
	private GoogleMap googleMap;
	private Button btnPath, btnList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_trafiq_route_map, null, false);

		MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.locationmap);
		btnPath = (Button) v.findViewById(R.id.btn_path);
		btnList = (Button) v.findViewById(R.id.btn_list);
		btnList.setOnClickListener(this);
		btnPath.setTextSize(25);
		googleMap = mapFragment.getMap();

		getArgumentData();
		
		return v;
	}

	private void setMap() {
		Log.e("log", "setupmap");
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(lattitude,longitude)).zoom(15)
				.build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(lattitude,longitude)).title(Stopage_Name);
		marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
		googleMap.addMarker(marker);
		
		CircleOptions circleOptions=new CircleOptions();
		circleOptions.center(new LatLng(lattitude,longitude));
		circleOptions.radius(500);
		circleOptions.strokeColor(Color.BLUE);
		circleOptions.strokeWidth(2);
		Circle circle=googleMap.addCircle(circleOptions);

		googleMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng latlng) {

				googleMap.setMyLocationEnabled(true);
			}
		});

	}

	private void getArgumentData() {
		Stopage_Name = getArguments().getString("stopage_name");
		lattitude = getArguments().getDouble("lat");
		longitude = getArguments().getDouble("lon");
		getActivity().getActionBar().setTitle(Stopage_Name);
		setMap();
		new GetStopageBusLocation().execute();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		try {
			Log.e("MSG", "on destroy called");
			MapFragment fragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.locationmap);
			if (fragment != null)
				getFragmentManager().beginTransaction().remove(fragment).commit();

		} catch (IllegalStateException e) {
			// handle this situation because you are necessary will get
			// an exception here :-(
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_path) {

		} else if (v.getId() == R.id.btn_list) {

			FragmentTrafiqData tdFragment=new FragmentTrafiqData();
			Bundle bundle=new Bundle();
			bundle.putInt("size",nBusList.size());
			Log.e("Size",""+nBusList.size());

			tdFragment.setArguments(bundle);
			FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
			fragmentTransaction.hide(TarfiqMapFragment.this);
			fragmentTransaction.add(R.id.content_frame,tdFragment);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
		}

	}

	private class GetStopageBusLocation extends AsyncTask<Void, Void, Void> {
		String msg = "";
		String filename = "get_bus_count_stopages.php";

		@Override
		protected Void doInBackground(Void... params) {
			Log.e("......", "hello");
			String url = Constants.URL_ROOT + filename;
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("lat", "" + lattitude));
			key.add(new BasicNameValuePair("lon", "" + longitude));
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}
			try {
				HttpResponse response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while ((line = br.readLine()) != null) {
					msg += line;
				}
				Log.e("......", msg);
				msg = msg.substring(0, msg.length() - 146);
				parseBusData(msg);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			InsertMarkerInMap();
			super.onPostExecute(result);
		}

		private void parseBusData(String json) {
			try {
				JSONObject jObject = new JSONObject(json);
				JSONObject j = jObject.getJSONObject("success");
				JSONArray jaArray = j.getJSONArray("buses");
				for (int i = 0; i < jaArray.length(); i++) {
					Log.e("length", "" + jaArray.length());
					JSONObject t = jaArray.getJSONObject(i);
					NearestBus nBus = new NearestBus(t.getDouble("distance"), t.getInt("bus_id"), t.getInt("Id"),
							t.getString("Name"), t.getLong("time"), t.getDouble("lat"), t.getDouble("lon"));
					nBusList.add(nBus);

				}
				
			} catch (JSONException e) {

				e.printStackTrace();
			}

		}

		private void InsertMarkerInMap() {
			for (int i = 0; i < nBusList.size(); i++) {
				MarkerOptions marker = new MarkerOptions().position(
						new LatLng(nBusList.get(i).getLattitude(),nBusList.get(i).getLongitude())).title(
						getDate(nBusList.get(i).getTimeInMilis(), "dd/MM/yyyy hh:mm:ss.SSS")).snippet(nBusList.get(i).getService_name());
				//marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				googleMap.addMarker(marker);
			}
		}

		public String getDate(long milliSeconds, String dateFormat) {
			// Create a DateFormatter object for displaying date in specified
			// format.
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

			// Create a calendar object that will convert the date and time
			// value in milliseconds to date.
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(milliSeconds);
			return formatter.format(calendar.getTime());

		}
	}
}

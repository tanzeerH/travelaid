package com.sd.travelaid.fragment;

import com.google.android.gms.internal.cp;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.sd.travelaid.R;
import com.sd.travelaid.util.Common;
import com.sd.travelaid.util.Constants;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpFragment extends Fragment implements OnClickListener {
	private EditText edtName, edtEmail, edtPass, edtConfirmPass;
	private Button btnSignup;
	private ProgressDialog pdDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.sign_up, container, false);
		btnSignup = (Button) v.findViewById(R.id.btn_signup);
		edtName = (EditText) v.findViewById(R.id.et_name);
		edtEmail = (EditText) v.findViewById(R.id.et_email);
		edtPass = (EditText) v.findViewById(R.id.et_pass);
		edtConfirmPass = (EditText) v.findViewById(R.id.et_cpass);
		btnSignup.setOnClickListener(this);
		return v;
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btn_signup) {
			String name = edtName.getText().toString();
			String email = edtEmail.getText().toString();
			String pass = edtPass.getText().toString();
			String cpass = edtConfirmPass.getText().toString();
			if (name.equals("") || email.equals("") || pass.equals("") || cpass.equals(""))
				toast("Field Empty.Please check");
			else if (!pass.equals(cpass))
				toast("Password mismatch.please check");
			else {
					ParseUser parseUser=new ParseUser();
					parseUser.setUsername(name);
					parseUser.setEmail(email);
					parseUser.setPassword(cpass);
					pdDialog=Common.createProgressDialog(getActivity());
					parseUser.signUpInBackground(new SignUpCallback() {
						
						@Override
						public void done(ParseException e) {
							pdDialog.dismiss();
							if(e==null)
							{
								//take him to profile
								toast("sign up success full");
							}
							else
							{
								alert("Error in Sign Up.");
							}
						}
					});
			}
		}
	}

	private void toast(String str) {
		Toast.makeText(getActivity(), str, Toast.LENGTH_LONG).show();
	}
	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		bld.create().show();
	}

}

package com.sd.travelaid.fragment;

import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.sd.travelaid.R;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LogInFragment extends Fragment implements OnClickListener{
	private EditText edtEmail,edtPass;
	private Button btnLogin,btnSignUp,btnFbLogin;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.log_in,null,false);
		btnFbLogin=(Button)v.findViewById(R.id.btn_fblogin);
		btnLogin=(Button)v.findViewById(R.id.btn_login);
		btnSignUp=(Button)v.findViewById(R.id.btn_signup);
		
		edtEmail=(EditText)v.findViewById(R.id.et_mail);
		edtPass=(EditText)v.findViewById(R.id.et_pass);
		
		btnFbLogin.setOnClickListener(this);
		btnLogin.setOnClickListener(this);
		btnSignUp.setOnClickListener(this);
		
		//facebook 
		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(getActivity(), null, statusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(getActivity());
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				OpenRequest openRequest = new OpenRequest(getActivity());
				openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
				openRequest.setPermissions("publish_actions");
				session.openForPublish(openRequest.setCallback(statusCallback));
			}
			
		}
		
		
		return v;
	}
	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			if (session.isOpened()) {

				for (int i = 0; i < session.getPermissions().size(); i++)
					Log.v("permissions", session.getPermissions().get(i));
				

			}

		}
	}
	

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_fblogin) {
			
			onClickLogin();
		} else if (v.getId() == R.id.btn_login) {
			String email=edtEmail.getText().toString();
			String pass=edtPass.getText().toString();
			if(email.equals("") || pass.equals(""))
			{
				toast("Field Empty.Please check.");
			}
			else
			{
				ParseUser.logInInBackground(email,pass,new LogInCallback() {
					
					@Override
					public void done(ParseUser user, ParseException e) {
						if(e==null)
						{
							FragmentManager fragmentManager=getFragmentManager();
							FragmentTransaction ft=fragmentManager.beginTransaction();
							ft.replace(R.id.content_frame,new ProfileFragment());
							ft.commit();
						}
						else
						{
						alert("Error in Login.");	
						}
						
					}
				});
			}
			
		} else if (v.getId() == R.id.btn_signup) {
				
			FragmentManager fragmentManager=getFragmentManager();
			FragmentTransaction ft=fragmentManager.beginTransaction();
			ft.replace(R.id.content_frame,new SignUpFragment());
			ft.commit();
		}
			
		
	}
	private void onClickLogin() {
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			OpenRequest openRequest = new OpenRequest(getActivity());
			openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
			openRequest.setPermissions("publish_actions");
			session.openForPublish(openRequest.setCallback(statusCallback));
		} else {
			Session.openActiveSession(getActivity(), true, statusCallback);
		}
	}
	private void toast(String str)
	{
		Toast.makeText(getActivity(),str,Toast.LENGTH_LONG).show();
	}
	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		bld.create().show();
	}
}

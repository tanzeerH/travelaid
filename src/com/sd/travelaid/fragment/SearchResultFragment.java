package com.sd.travelaid.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sd.travelaid.R;
import com.sd.travelaid.adapter.BusServiceListAdapter;
import com.sd.travelaid.model.BusRoute;
import com.sd.travelaid.util.Common;
import com.sd.travelaid.util.Constants;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SearchResultFragment  extends Fragment{
	private ListView listView;
	private BusServiceListAdapter adapter;
	private ArrayList<BusRoute> list=new ArrayList<BusRoute>();
	private ProgressDialog pdDialog;
	private int start_id=-1;
	private int end_id=-1;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_services,null,false);
		pdDialog=Common.createProgressDialog(getActivity());
		//pdDialog.show();
		listView=(ListView)v.findViewById(R.id.listView1);
		//initBusSerVices();
		adapter=new BusServiceListAdapter(getActivity(),R.layout.bus_list_row,list);
		listView.setAdapter(adapter);
		getArgumentData();
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				
				BusRouteMapFragment buFragment=new BusRouteMapFragment();
				Bundle bundle=new Bundle();
				bundle.putString("service_name",list.get(position).getName());
				bundle.putInt("route_id",list.get(position).getRouteId());
				bundle.putInt("service_id",list.get(position).getServiceId());
				buFragment.setArguments(bundle);
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				fragmentTransaction.hide(SearchResultFragment.this);
				fragmentTransaction.add(R.id.content_frame,buFragment);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});
		new GetSearchResult().execute();
		return v;
	}
	private void getArgumentData()
	{
		Bundle b=getArguments();
		start_id=b.getInt("start_id");
		end_id=b.getInt("end_id");
	}
	
	private class GetSearchResult extends AsyncTask<Void,Void,Void>
	{

		String msg="";
		String filename="get_search_result.php";
		@Override
		protected Void doInBackground(Void... params) {
			
			String url=Constants.URL_ROOT+filename;
			DefaultHttpClient client= new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("start_id", "" + start_id));
			key.add(new BasicNameValuePair("end_id", "" + end_id));
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {
				
				e.printStackTrace();
			}
			try {
				HttpResponse response=client.execute(httpPost);
				HttpEntity entity=response.getEntity();
				InputStream is= entity.getContent();
				BufferedReader br=new BufferedReader(new InputStreamReader(is));
				String line="";
				while((line=br.readLine())!=null)
				{
					msg+=line;
				}
				Log.e("......",msg);
				msg=msg.substring(0,msg.length()-146);
				parseJson(msg);
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			adapter.notifyDataSetChanged();
			pdDialog.dismiss();
			super.onPostExecute(result);
			
		}
		
	}
	private void parseJson(String json)
	{
		try {
			JSONObject jObject=new JSONObject(json);
			JSONObject j=jObject.getJSONObject("success");
			JSONArray jaArray=j.getJSONArray("bus_routes");
			for(int i=0;i<jaArray.length();i++)
			{
				Log.e("length",""+jaArray.length());
				JSONObject t=jaArray.getJSONObject(i);
				BusRoute route=new BusRoute(t.getString("name"),t.getInt("route_id"),t.getInt("id"),"","");
				list.add(route);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

package com.sd.travelaid.fragment;

import com.sd.travelaid.R;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentTrafiqData extends Fragment{

	private TextView tvNum;
	private TextView tvstatus;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_trafiq_data,null,false);
		tvNum=(TextView)v.findViewById(R.id.tv_number);
		tvstatus=(TextView)v.findViewById(R.id.tv_data);
		//Bundle bundle=getArguments();
		int num=getArguments().getInt("size");
		Log.e(".......",""+num);
		tvNum.setText("Total " + num +" Bus nearest this area.");
		if(num<3 && num>=1)
		{
			tvstatus.setText("Trafiq status: Normal");
		}
		if(num>3)
		{
			tvstatus.setText("Trafiq status: Busy");
		}
		if(num==0)
		{
			tvstatus.setText("Trafiq status: Trafiq can not be determined. Low data flow.");
		}
		return v;
	}
}

package com.sd.travelaid.model;

public class Bus {
	private int id;
	private double latitude;
	private double longitude;
	private long timiInMili;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public Bus(int id, double latitude, double longitude, long timiInMili) {
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.timiInMili = timiInMili;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public long getTimiInMili() {
		return timiInMili;
	}

	public void setTimiInMili(long timiInMili) {
		this.timiInMili = timiInMili;
	}
}

package com.sd.travelaid.model;

public class BusRoute {
	private String name;
	private int route_id;
	private int service_id;
	private String startLocation;
	private String finishLocation;
	
	public BusRoute(String name,int rid,int sid,String start,String finish) {
		this.name=name;
		this.route_id=rid;
		this.service_id=sid;
		this.startLocation=start;
		this.finishLocation=finish;
		
	}
	public String getName()
	{
		return this.name;
	}
	public int getRouteId()
	{
		return this.route_id;
	}
	public int getServiceId()
	{
		return this.service_id;
	}
	public String getStartLocation()
	{
		return this.startLocation;
	}
	public String getFinishLocation()
	{
		return this.finishLocation;
	}
	



}

package com.sd.travelaid.model;

public class Comment {

	private String name;
	private String cmnt;
	public Comment(String n,String c) {
		this.name=n;
		this.cmnt=c;
	}
	public String getUserName()
	{
		return this.name;
	}
	public String getComment()
	{
		return this.cmnt;
	}
}

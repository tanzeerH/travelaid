package com.sd.travelaid.model;

public class MenuItem {
	
	private String name;
	private int iconRes;
	
	public MenuItem(String name,int iconRes) {
		this.name=name;
		this.iconRes=iconRes;
		
	}
	public String getName()
	{
		return this.name;
	}
	public int getIconRes()
	{
		return this.iconRes;
	}


}

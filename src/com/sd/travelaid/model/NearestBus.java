package com.sd.travelaid.model;

public class NearestBus {
	private double distance;
	private int bus_id;
	private int service_id;
	private String service_name;
	private long timeInMilis;
	private double lattitude;
	private double longitude;
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public int getBus_id() {
		return bus_id;
	}
	public void setBus_id(int bus_id) {
		this.bus_id = bus_id;
	}
	public int getService_id() {
		return service_id;
	}
	public void setService_id(int service_id) {
		this.service_id = service_id;
	}
	public String getService_name() {
		return service_name;
	}
	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
	public long getTimeInMilis() {
		return timeInMilis;
	}
	public void setTimeInMilis(long timeInMilis) {
		this.timeInMilis = timeInMilis;
	}
	public double getLattitude() {
		return lattitude;
	}
	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public NearestBus(double distance, int bus_id, int service_id, String service_name, long timeInMilis,
			double lattitude, double longitude) {
		super();
		this.distance = distance;
		this.bus_id = bus_id;
		this.service_id = service_id;
		this.service_name = service_name;
		this.timeInMilis = timeInMilis;
		this.lattitude = lattitude;
		this.longitude = longitude;
	}
	
}

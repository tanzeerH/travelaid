package com.sd.travelaid.model;

public class Stopage {
	
	private int id;
	private String name;
	private double latitude;
	private double longitude;
	public Stopage(int id,String name,double lat,double lon) {
		this.id=id;
		this.name=name;
		this.latitude=lat;
		this.longitude=lon;
	}
	public int getId()
	{
		return this.id;
	}
	public String getName()
	{
		return this.name;
	}
	public double getLatitude()
	{
		return this.latitude;
	}
	public double getLongitude()
	{
		return this.longitude;
	}
}	

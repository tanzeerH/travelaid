package com.sd.comparator;

import java.util.Comparator;

import com.sd.travelaid.model.Stopage;

public class SortStopagesByName  implements Comparator<Stopage>{

	@Override
	public int compare(Stopage s1, Stopage s2) {
		return s1.getName().compareTo(s2.getName());
		
	}
	

}
